#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <iosfwd>
#include <list>
#include <math.h>
#include <process.h>
#include <psapi.h>
#include <shlwapi.h>
#include <sstream>
#include <string>
#include <time.h>
#include <tlhelp32.h>
#include <valarray>
#include <vector>
#include <winbase.h>
#include <windef.h>
#include <winnt.h>
#include <winuser.h>
#include <queue>
#include <direct.h>	
#include <errno.h>
#include <io.h>
#include <tchar.h>
#include <iomanip>
#include <unordered_map>
#include <random>
#include <iterator>
#include <algorithm>
#include <map>
#include <fstream>
#include <regex>
#include <deque>
#include <random>
#include <mbstring.h>
#include <malloc.h>
#include <wchar.h>
#include <conio.h>
#include <cctype>
#include <memory>
#include <cstdarg>
#include <type_traits>
#include <chrono>
#include <thread>
#include <mutex>
#include <future>
#include <filesystem>

using namespace std;

enum D2C_PlayerFlags
{
	PLRFLAG_NEWBIE = 0x01,
	PLRFLAG_HARDCORE = 0x04,
	PLRFLAG_DEAD = 0x08,
	PLRFLAG_EXPANSION = 0x20,
	PLRFLAG_LADDER = 0x40,
};

const unsigned MAX_CHARNAME_LEN = 16;
const unsigned MAX_USERNAME_LEN = 16;
const int MAX_REALMNAME_LEN = 32;
#define D2CHARINFO_PORTRAIT_PADSIZE		30

#define D2CHARINFO_MAGICWORD			0x12345678

#define BIT_TST_FLAG(n,f)		( ((n) & (f)) ? 1: 0 )

#define BIT_CLR_FLAG(n,f)	( (n) &= ~(f) )
#define BIT_SET_FLAG(n,f)	( (n) |= (f) )

#define BIT_SET_CLR_FLAG(n, f, v)	( (v)?(BIT_SET_FLAG(n,f)):(BIT_CLR_FLAG(n,f)) )

#define D2CHARINFO_STATUS_FLAG_LADDER			0x40
#define D2CHARINFO_PORTRAIT_PADBYTE			0xff

#define	charstatus_get_ladder(status)		BIT_TST_FLAG(status, D2CHARINFO_STATUS_FLAG_LADDER)
#define charstatus_set_ladder(status,n)		BIT_SET_CLR_FLAG(status, D2CHARINFO_STATUS_FLAG_LADDER, n)

void LogMsg(char* Msg...)
{
	va_list arguments;
	va_start(arguments, Msg);

	int len = _vscprintf(Msg, arguments) + 1;
	char* text = new char[len];
	vsprintf_s(text, len, Msg, arguments);
	va_end(arguments);

	FILE* plik;
	fopen_s(&plik, "test.log", "a");
	if (plik)
	{
		fprintf(plik, "%s\n", text);
		fclose(plik);
	}
	delete[] text;
}

void Logcharsave(char* Msg...)
{
	va_list arguments;
	va_start(arguments, Msg);

	int len = _vscprintf(Msg, arguments) + 1;
	char* text = new char[len];
	vsprintf_s(text, len, Msg, arguments);
	va_end(arguments);

	FILE* plik;
	fopen_s(&plik, "charsave.log", "a");
	if (plik)
	{
		fprintf(plik, "%s\n", text);
		fclose(plik);
	}
	delete[] text;
}

void Logcharinfo(char* Msg...)
{
	va_list arguments;
	va_start(arguments, Msg);

	int len = _vscprintf(Msg, arguments) + 1;
	char* text = new char[len];
	vsprintf_s(text, len, Msg, arguments);
	va_end(arguments);

	FILE* plik;
	fopen_s(&plik, "charinfo.log", "a");
	if (plik)
	{
		fprintf(plik, "%s\n", text);
		fclose(plik);
	}
	delete[] text;
}

void Logcharsave_error(char* Msg...)
{
	va_list arguments;
	va_start(arguments, Msg);

	int len = _vscprintf(Msg, arguments) + 1;
	char* text = new char[len];
	vsprintf_s(text, len, Msg, arguments);
	va_end(arguments);

	FILE* plik;
	fopen_s(&plik, "charsave_error.log", "a");
	if (plik)
	{
		fprintf(plik, "%s\n", text);
		fclose(plik);
	}
	delete[] text;
}

void Logcharinfo_error(char* Msg...)
{
	va_list arguments;
	va_start(arguments, Msg);

	int len = _vscprintf(Msg, arguments) + 1;
	char* text = new char[len];
	vsprintf_s(text, len, Msg, arguments);
	va_end(arguments);

	FILE* plik;
	fopen_s(&plik, "charinfo_error.log", "a");
	if (plik)
	{
		fprintf(plik, "%s\n", text);
		fclose(plik);
	}
	delete[] text;
}

vector<string> get_all_files_names_within_folder(string folder)
{
	vector<string> names;
	string search_path = folder + "/*.*";
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				names.push_back(fd.cFileName);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
	return names;
}

#pragma pack(1)
struct SaveSkillKey
{
	WORD nSkill;			//0x00
	union
	{
		struct
		{
			BYTE nKey;		//0x02
			BYTE nKeyEx;	//0x03
		};

		WORD nItemSlot;		//0x02
	};
};

struct MercSaveData
{
	DWORD dwFlags;			//0x0AF
	DWORD dwSeed;			//0x0B3
	WORD nName;				//0x0B7
	WORD nType;				//0x0B9
	DWORD dwExperience;		//0x0BB
};

struct SaveHeader
{
	DWORD dwHeaderMagic;					//0x0
	DWORD dwVersion;						//0x4
	DWORD dwSize;							//0x8
	DWORD dwChecksum;						//0xC
	DWORD dwWeaponSet;						//0x10
	char szName[16];						//0x14
	union
	{
		DWORD dwSaveFlags;					//0x24
		struct
		{
			BYTE bSaveFlags;				//0x24
			BYTE bActDone;					//0x25
			BYTE Empty1;					//0x26
			BYTE Empty2;					//0x27
		};
	};
	BYTE nClass;							//0x28
	BYTE nStats;							//0x29
	BYTE nSkills;							//0x2A
	BYTE nLevel;							//0x2B
	DWORD dwCreateTime;						//0x2C
	DWORD dwLasTime;						//0x30
	DWORD dwPlayTime;						//0x34
	SaveSkillKey SkillKeys[16];				//0x38
	SaveSkillKey ButtonSkills[4];			//0x78
	BYTE nComponent[16];					//0x88
	BYTE nCompColor[16];					//0x98
	BYTE nTown[3];							//0xA8
	DWORD dwMapSeed;						//0xAB
	MercSaveData MercSaveData;				//0xAF
	//Realm + Mod D2VN
	BYTE nRealmData[16];					//0xBF
	BYTE nSaveField;						//0xCF
	DWORD dwLastLevel;						//0xD0
	DWORD dwLastTown;						//0xD4
	BYTE nLastDifficulty;					//0xD8
	BYTE AutoPickGold;						//0xD9
	char PassChar[32];						//0xDA
	BYTE DrawDmg;							//0xFA
	BYTE DrawBuff;							//0xFB
	BYTE DrawDmgAvoid;						//0xFC
	BYTE StatusMoveItem;					//0xFD
	BYTE isVIP;								//0xFE
	BYTE SaveStashMySQL;					//0xFF
	char szIP[16];
	char szGameName[32];
	BYTE RemoveItemInSock;
	int timestamp;
	BYTE turnsCow;
	BYTE turnsBossCow;
	BYTE AddTurn;
	DWORD SizeStash;
	DWORD SizeSaveEx;
	BYTE nLimitPoint[4];
	BYTE nBonus2022;
	BYTE Unk1[10];
};

struct SaveHeaderEx // size 0x2410
{
	DWORD Header;				//0x00
	DWORD Header2;				//0x04
	DWORD nSizeTotal;			//0x08
	DWORD nSizeItem;			//0x0C
	DWORD nSizeCube[7];			//0x10
	DWORD nSizeInv[7];			//0x2C
	char szIp[64];				//0x48
	char szAcc[64];				//0x88
	BYTE DrawDmgStyle;			//0xC8
	BYTE DrawDmgFading;			//0xC9
	BYTE DrawDmgAvoidStyle;		//0xCA
	BYTE DrawDmgAvoidFading;	//0xCB
	WORD nMisc[300];			//0xCC
	int nPlayerBonus[10];		//0x324
	char szGameName[64];		//0x34C
	char szGamePass[64];		//0x38C
	BYTE DataSomething[7236];	//0x3CC
	BYTE DataItemCursor[1024];	//0x2010	
};

struct t_d2charinfo_header
{
	int		magicword;	/* static for check */
	int		version;	/* charinfo file version */
	int		create_time;	/* character creation time */
	int		last_time;	/* character last access time */
	int		checksum;
	int      total_play_time;        /* total in game play time */
	int		reserved[6];
	char	charname[MAX_CHARNAME_LEN];
	char	account[MAX_USERNAME_LEN];
	char	realmname[MAX_REALMNAME_LEN];
};

struct t_d2charinfo_summary
{
	int		experience;
	int		charstatus;
	int		charlevel;
	int		charclass;
};

struct t_d2charinfo_portrait
{
	short        header;	/* 0x84 0x80 */
	BYTE         gfx[11];
	BYTE         chclass;
	BYTE         color[11];
	BYTE         level;
	BYTE         status;



	BYTE         u1[3];
	BYTE			ladder;	/* need not be 0xff and 0 to make character displayed as ladder character */
	/* client only check this bit */
	BYTE         u2[2];
	BYTE         end;	/* 0x00 */
} ;

struct t_d2charinfo_file
{
	t_d2charinfo_header		header;
	t_d2charinfo_portrait	portrait;
	BYTE         			pad[D2CHARINFO_PORTRAIT_PADSIZE];
	t_d2charinfo_summary	summary;
};

#pragma pack()

string Hex2String(void *const data, const size_t dataLength)
{
	string dest;

	unsigned char* byteData = reinterpret_cast<unsigned char*>(data);
	stringstream hexStringStream;

	hexStringStream << hex << setfill('0');
	for (size_t index = 0; index < dataLength; ++index)
		hexStringStream << setw(2) << static_cast<int>(byteData[index]);
	dest = hexStringStream.str();

	return dest;
}

void StingHex2Data(const string &in, void *const data)
{
	size_t          length = in.length();
	unsigned char   *byteData = reinterpret_cast<unsigned char*>(data);

	stringstream hexStringStream;
	hexStringStream >> hex;
	for (size_t strIndex = 0, dataIndex = 0; strIndex < length; ++dataIndex)
	{
		const char tmpStr[3] = { in[strIndex++], in[strIndex++], 0 };

		hexStringStream.clear();
		hexStringStream.str(tmpStr);

		int tmpValue = 0;
		hexStringStream >> tmpValue;
		byteData[dataIndex] = static_cast<unsigned char>(tmpValue);
	}
}

int GetCountFindString(string data, string find)
{
	int count = 0;
	string::size_type i = data.find(find);
	while (i != string::npos)
	{
		++count;
		i = data.find(find, i + find.length());
	}
	return count;
}

#pragma pack(push,1)
struct fingerprintcontainer 
{
	int first : 7;
	int data : 32;
	int last : 1;
};
fingerprintcontainer* fpcptr;

struct itemtypecontainer 
{
	int first : 4;
	int one : 8;
	int two : 8;
	int three : 8;
	int four : 8;
	int last : 4;
};
itemtypecontainer* itcptr;

struct item 
{
	char szAcc[32];
	char charname[32];
	char szIp[32];
	int number;
	int base;
	int length;
	char* itemtype;
	unsigned long fingerprint;
	item* next;
};

struct character 
{
	char* charname;
	char* filename;
	item* firstitem;
};
#pragma pack()

int debug = 0, verbose = 0, overalldupes = 0;
character* parsefile(const char *filename)
{
	FILE *file;
	char *buf, *data;
	int size, i, bufsize, jmcount = 0;
	short* itemcount = NULL, myitemcount = 0;
	int itemlist_base = -1, itemnumber = 0;
	character* theChar = (character*)malloc(sizeof(character));
	memset(theChar, 0, sizeof(character));
	item** theItemlist = NULL;

	size = strlen(filename) + 1;
	theChar->filename = (char*)malloc(size);
	strncpy(theChar->filename, filename, size);

	// 15 bytes for charname + nullbyte
	theChar->charname = (char*)malloc(16);

	file = fopen(filename, "rb");
	fseek(file, 0, SEEK_END);
	bufsize = ftell(file);
	fseek(file, 0, SEEK_SET);
	if (bufsize < 8192)
	{
		fclose(file);
		return NULL;
	}

	buf = (char*)malloc(bufsize);
	memset(buf, 0, bufsize);

	size = fread(buf, 1, bufsize + 1, file);
	fclose(file);

	string szAcc, szChar, szIP;
	SaveHeaderEx* pSaveHeaderEx = nullptr;
	SaveHeader* pSaveHeader = (SaveHeader*)buf;
	if (pSaveHeader->SizeSaveEx)
	{
		BYTE* DataSaveEx = (BYTE*)malloc(pSaveHeader->SizeSaveEx);
		memset(DataSaveEx, 0, pSaveHeader->SizeSaveEx);
		memcpy(DataSaveEx, buf + (pSaveHeader->dwSize + pSaveHeader->SizeStash), pSaveHeader->SizeSaveEx);
		pSaveHeaderEx = (SaveHeaderEx*)DataSaveEx;
	}
	szAcc = pSaveHeaderEx ? pSaveHeaderEx->szAcc : "";
	szChar = pSaveHeader->szName;
	szIP = pSaveHeader->szIP;

	strncpy(theChar->charname, &buf[20], 16);

	if (verbose != -1)
	{
		printf("[%s](%s) %s\n", szIP.c_str(), szAcc.c_str(), szChar.c_str());
		//LogMsg("[%s](%s) %s", szIP.c_str(), szAcc.c_str(), szChar.c_str());

		//printf("OK: Read %4d bytes of character data for \"%s\" from \"%s\"...\n", size, theChar->charname, theChar->filename);
		//LogMsg("OK: Read %4d bytes of character data for \"%s\" from \"%s\"...\n", size, theChar->charname, theChar->filename);
	}

	for (i = 0; i<bufsize - 1; i++) 
	{
		if (buf[i] == 'J' && buf[i + 1] == 'M') 
		{
			jmcount++;
			if (itemcount != NULL) 
			{
				if (itemnumber > *itemcount) 
				{
					if (debug) 
						LogMsg("DEBUG: itemnumber is larger than expected (%d>%d). Enlarging buffer.", itemnumber, *itemcount);

					theItemlist = (item**)realloc(theItemlist, (itemnumber + 1) * sizeof(item*));
					if (theItemlist == NULL) 
					{
						LogMsg("Memory allocation error (realloc())", filename);
					}
				}
			}

			if (itemlist_base == -1) 
			{
				if (i + 5 >= bufsize || buf[i + 4] != 'J' || buf[i + 5] != 'M') 
				{
					if (debug) 
						LogMsg("DEBUG: %s: Found first JM at buf[%d] but it is not followed by another JM, so its not the itemlist base! Skipping.", filename, i);
					continue;
				}
				itemcount = (short*)&buf[i + 2];

				if (debug) 
					LogMsg("DEBUG: %s: Found first JM: Itemlist base is buf[%d], following %hd items (JM 0x%x 0x%x)", filename, i, *itemcount, buf[i + 2], buf[i + 3]);

				itemlist_base = i;
				if ((theItemlist = (item**)malloc((*itemcount + 1) * sizeof(item*))) == NULL) 
				{
					LogMsg("Memory allocation error", filename);
				}
			}
			else if (buf[i + 2] == 0 && buf[i + 3] == 0) 
			{
				if (*itemcount != 0) 
				{
					if (itemnumber > 0) 
					{
						theItemlist[itemnumber - 1]->length = (i)-theItemlist[itemnumber - 1]->base;
						if (debug) 
							LogMsg("DEBUG: %s: Found itembase %2d at buf[%04d], length: %2d", filename, itemnumber - 1, theItemlist[itemnumber - 1]->base, theItemlist[itemnumber - 1]->length);
					}
				}
				if (debug) 
					LogMsg("DEBUG: %s: Found last JM: Itemlist ends at buf[%d]", filename, i - 1);
				break;
			}
			else
			{
				data = &buf[i];
				itcptr = (itemtypecontainer*)&data[9];
				theItemlist[itemnumber] = (item*)malloc(sizeof(item));
				theItemlist[itemnumber]->itemtype = (char*)malloc(5);
				sprintf(theItemlist[itemnumber]->itemtype, "%c%c%c%c", itcptr->one, itcptr->two, itcptr->three, itcptr->four);
				theItemlist[itemnumber]->number = itemnumber;
				theItemlist[itemnumber]->next = NULL;
				theItemlist[itemnumber]->fingerprint = 0;
				theItemlist[itemnumber]->base = i;
				theItemlist[itemnumber]->length = 0;
				strcpy(theItemlist[itemnumber]->szAcc, szAcc.c_str());
				strcpy(theItemlist[itemnumber]->charname, szChar.c_str());
				strcpy(theItemlist[itemnumber]->szIp, szIP.c_str());
				if (itemnumber == 0) 
					theChar->firstitem = theItemlist[0];
				else 
				{
					myitemcount++;
					theItemlist[itemnumber - 1]->length = theItemlist[itemnumber]->base - theItemlist[itemnumber - 1]->base;
					theItemlist[itemnumber - 1]->next = theItemlist[itemnumber];

					if (debug)
						LogMsg("DEBUG: %s: Found itembase %2d at buf[%04d], length: %2d", filename, itemnumber - 1, theItemlist[itemnumber - 1]->base, theItemlist[itemnumber - 1]->length);
				}
				itemnumber++;
			}
		}

	}


	// if the char does not have any items: set the first item to zero
	if (jmcount == 0 && itemcount == NULL) 
		theChar->firstitem = NULL;

	for (i = 0; i < myitemcount; i++) 
	{
		// standard items are 14 bytes long, everything thats longer is an extended item
		// 19: dunno
		// 20: -
		// 21: itemtype=box (maybe cube?)
		// 22: key
		// 23: ibk, tbk (identify/townportal??)
		// HACK: again, not really sure about that stuff here.. we just ignore everything
		// that is shorter than 23 bytes for now
		if (theItemlist[i]->length >= 23) 
		{
			// read the bitstructure at offset 13
			data = &buf[theItemlist[i]->base];
			fpcptr = (fingerprintcontainer*)&data[13];
			theItemlist[i]->fingerprint = fpcptr->data;

			if (debug) 
				LogMsg("DEBUG: Item %3d is an extended item and therefore has a Fingerprint (0x%lx) (type=%s)", i, theItemlist[i]->fingerprint, theItemlist[i]->itemtype);
		}
	}

	if (debug) 
		LogMsg("DEBUG: End of file reached for %s", filename);

	free(buf);
	return theChar;
}

struct ItemData
{
	string szAcc;
	string charname;
	string szIp;
	string szAcc2;
	string charname2;
	string szIp2;
	int number;
	unsigned long fingerprint;
	string szCode;
};

vector<ItemData> pItemData;
vector<string> item_filter;
int Chose = 0;
void dumpchar(character* theChar)
{
	pItemData.clear();

	item* ptr;
	if ((ptr = theChar->firstitem) == NULL)
		return;

	while (ptr != NULL) 
	{
		if (ptr->fingerprint != 0)
		{
			if (debug) 
				LogMsg(" Adr: %p, next: %p,", (void*)ptr, (void*)ptr->next);

			if (Chose == 0)
			{
				printf("[%s][(%s)%s] Number: %d, Fingerprint: 0x%08lx, iCode: %s\n", ptr->szIp, ptr->szAcc, ptr->charname, ptr->number, ptr->fingerprint, ptr->itemtype);
				LogMsg("[%s][(%s)%s]Number: %d, Fingerprint: 0x%08lx, iCode: %s", ptr->szIp, ptr->szAcc, ptr->charname, ptr->number, ptr->fingerprint, ptr->itemtype);
			}

			ItemData gwItemData = { ptr->szAcc, ptr->charname, ptr->szIp, "", "", "", ptr->number, ptr->fingerprint, ptr->itemtype };
			pItemData.push_back(gwItemData);

			//printf("Number: %d, Fingerprint: 0x%08lx, iCode: %s\n", ptr->number, ptr->fingerprint, ptr->itemtype);
			//LogMsg("Number: %d, Fingerprint: 0x%08lx, iCode: %s", ptr->number, ptr->fingerprint, ptr->itemtype);
		}
		ptr = ptr->next;
	}

	if (Chose == 1)
	{
		if (!pItemData.empty())
		{
			for (int i = 0; i < pItemData.size() - 1; i++)
			{
				for (int j = i + 1; j < pItemData.size(); j++)
				{
					if (pItemData[i].fingerprint == pItemData[j].fingerprint)
					{
						string itemcode(pItemData[i].szCode);
						if (itemcode.length())
						{
							if (itemcode.find("hp1") != string::npos ||
								itemcode.find("hp2") != string::npos ||
								itemcode.find("hp3") != string::npos ||
								itemcode.find("hp4") != string::npos ||
								itemcode.find("hp5") != string::npos ||
								itemcode.find("mp1") != string::npos ||
								itemcode.find("mp2") != string::npos ||
								itemcode.find("mp3") != string::npos ||
								itemcode.find("mp4") != string::npos ||
								itemcode.find("mp5") != string::npos ||
								itemcode.find("rvl") != string::npos ||
								itemcode.find("box") != string::npos ||
								itemcode.find("tbk") != string::npos ||
								itemcode.find("vps") != string::npos ||
								itemcode.find("@qv") != string::npos ||
								itemcode.find("key") != string::npos ||
								itemcode.find("aqv") != string::npos ||
								itemcode.find("yps") != string::npos ||
								itemcode.find("gpm") != string::npos ||
								itemcode.find("tes") != string::npos ||
								itemcode.find("wms") != string::npos ||
								itemcode.find("fed") != string::npos ||
								itemcode.find("hc1") != string::npos ||
								itemcode.find("hc2") != string::npos ||
								itemcode.find("hc3") != string::npos ||
								itemcode.find("hc4") != string::npos ||
								itemcode.find("hc5") != string::npos ||
								itemcode.find("hc6") != string::npos ||
								itemcode.find("hc7") != string::npos ||
								itemcode.find("hc8") != string::npos ||
								itemcode.find("hc9") != string::npos)
								continue;
						}

						LogMsg("[%s] (%s) %s: Number: %d, Fingerprint: 0x%08lx, iCode: %s",
							pItemData[i].szIp.c_str(),
							pItemData[i].szAcc.c_str(),
							pItemData[i].charname.c_str(),
							pItemData[i].number,
							pItemData[i].fingerprint,
							pItemData[i].szCode.c_str());
						pItemData.erase(pItemData.begin() + i);
					}
				}
			}
		}
	}
}

void char_compare(character* one, character* two)
{
	pItemData.clear();
	item *itemone, *itemtwo;

	if ((itemone = one->firstitem) == NULL)
		return;

	while (itemone != NULL) 
	{
		itemtwo = two->firstitem;
		while (itemtwo != NULL) 
		{
			if (itemone->fingerprint == itemtwo->fingerprint && itemone->fingerprint != 0) 
			{
				// not really sure about that. ibk and tbk seem to be tp and id books
				// not sure why they seem to be extended items either.. just ignore them for now
				if (strcmp(itemone->itemtype, "ibk ") == 0 || strcmp(itemone->itemtype, "tbk ") == 0) 
				{
					itemtwo = itemtwo->next;
					continue;
				}

				if (Chose == 0)
				{
					printf("POSSIBLE DUPE: \"%s\" and \"%s\" both have an item of type \"%s\" with fingerprint 0x%lx\n", one->charname, two->charname, itemone->itemtype, itemone->fingerprint);
					LogMsg("POSSIBLE DUPE: \"%s\" and \"%s\" both have an item of type \"%s\" with fingerprint 0x%lx", one->charname, two->charname, itemone->itemtype, itemone->fingerprint);
					overalldupes++;
				}

				ItemData gwItemData = { itemone->szAcc, itemone->charname, itemone->szIp, itemtwo->szAcc, itemtwo->charname, itemtwo->szIp, itemone->number, itemone->fingerprint, itemone->itemtype };
				pItemData.push_back(gwItemData);
			}
			itemtwo = itemtwo->next;
		}
		itemone = itemone->next;
	}

	if (Chose == 1)
	{
		if (!pItemData.empty())
		{
			for (int i = 0; i < pItemData.size() - 1; i++)
			{
				for (int j = i + 1; j < pItemData.size(); j++)
				{
					if (pItemData[i].fingerprint == pItemData[j].fingerprint)
					{
						string itemcode(pItemData[i].szCode);
						if (itemcode.length())
						{
							if (itemcode.find("hp1") != string::npos ||
								itemcode.find("hp2") != string::npos ||
								itemcode.find("hp3") != string::npos ||
								itemcode.find("hp4") != string::npos ||
								itemcode.find("hp5") != string::npos ||
								itemcode.find("mp1") != string::npos ||
								itemcode.find("mp2") != string::npos ||
								itemcode.find("mp3") != string::npos ||
								itemcode.find("mp4") != string::npos ||
								itemcode.find("mp5") != string::npos ||
								itemcode.find("rvl") != string::npos ||
								itemcode.find("box") != string::npos ||
								itemcode.find("tbk") != string::npos ||
								itemcode.find("vps") != string::npos ||
								itemcode.find("@qv") != string::npos ||
								itemcode.find("key") != string::npos ||
								itemcode.find("aqv") != string::npos ||
								itemcode.find("yps") != string::npos ||
								itemcode.find("gpm") != string::npos ||
								itemcode.find("tes") != string::npos ||
								itemcode.find("wms") != string::npos ||
								itemcode.find("fed") != string::npos ||
								itemcode.find("hc1") != string::npos ||
								itemcode.find("hc2") != string::npos ||
								itemcode.find("hc3") != string::npos ||
								itemcode.find("hc4") != string::npos ||
								itemcode.find("hc5") != string::npos ||
								itemcode.find("hc6") != string::npos ||
								itemcode.find("hc7") != string::npos ||
								itemcode.find("hc8") != string::npos ||
								itemcode.find("hc9") != string::npos)
								continue;
						}

						LogMsg("[%s] (%s) %s -> [%s] (%s) %s : iCode \"%s\" with fingerprint 0x%lx",
							pItemData[i].szIp.c_str(),
							pItemData[i].szAcc.c_str(),
							pItemData[i].charname.c_str(),
							pItemData[i].szIp2.c_str(),
							pItemData[i].szAcc2.c_str(),
							pItemData[i].charname2.c_str(),
							pItemData[i].szCode.c_str(),
							pItemData[i].fingerprint);

						pItemData.erase(pItemData.begin() + i);
					}
				}
			}
		}
	}
}

DWORD __fastcall SAVEFILE_CalculateChecksum(BYTE* pSaveFile, int nSize)
{
	DWORD dwChecksum = 0;

	for (int i = 0; i < nSize; ++i)
	{
		dwChecksum = (dwChecksum << 1) + pSaveFile[i] + (dwChecksum & 0x80000000 ? 1 : 0);
	}

	return dwChecksum;
}

int d2charsave_checksum(unsigned char const * data, unsigned int len, unsigned int offset)
{
	int		checksum;
	unsigned int	i;
	unsigned int	ch;

	if (!data) return 0;
	checksum = 0;
	for (i = 0; i<len; i++) {
		if (i >= offset && i<offset + sizeof(int)) ch = 0;
		else ch = *data;
		ch += (checksum<0);
		checksum = 2 * checksum + ch;
		data++;
	}
	return checksum;
}

void getSubdirs(vector<string>& output, const string& path)
{
	WIN32_FIND_DATA findfiledata;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	char fullpath[MAX_PATH];
	GetFullPathName(path.c_str(), MAX_PATH, fullpath, 0);
	string fp(fullpath);

	hFind = FindFirstFile((LPCSTR)(fp + "\\*").c_str(), &findfiledata);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((findfiledata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0
				&& (findfiledata.cFileName[0] != '.'))
			{
				output.push_back(findfiledata.cFileName);
			}
		} while (FindNextFile(hFind, &findfiledata) != 0);
	}
}

void getSubdirsRecursive(vector<string>& output, const string& path, const string& prependStr)
{
	vector<string> firstLvl;
	getSubdirs(firstLvl, path);
	for (vector<string>::iterator i = firstLvl.begin(); i != firstLvl.end(); ++i)
	{
		output.push_back(prependStr + *i);
		getSubdirsRecursive(output, path + string("\\") + *i + string("\\"), prependStr + *i + string("\\"));
	}
}

int ChoseChar = 0;
int main()
{
	label1:
	{
		printf("0: [Check Dupe] Only Char\n");
		printf("1: [Check Dupe] Char vs Char\n");
		printf("3: [charsave] Ladder To Non-Ladder\n");
		printf("4: [charsave] Non-Ladder To Ladder\n");
		printf("5: [charinfo] Ladder To Non-Ladder\n");
		printf("6: [charinfo] Non-Ladder To Ladder\n");
		printf("7: [Rename] charsave\n");
		printf("8: [Rename] charinfo\n");
		printf("Chose: ");

		cin >> ChoseChar;
		system("cls");
	}

	if (ChoseChar >= 0 && ChoseChar <= 1)
	{
		printf("0: Not Item Filter\n");
		printf("1: Item filter\n");
		printf("3: Return\n");
		printf("Chose: ");

		cin >> Chose;

		if (Chose == 3)
		{
			system("cls");
			goto label1;
			return 0;
		}
	}

	int nSokytuthem;
	string addtext;
	if (ChoseChar == 7 || ChoseChar == 8)
	{
		printf("so vi tri can them: ");
		cin >> nSokytuthem;

		printf("text can them: ");
		cin >> addtext;
	}

	character* charlist[10000] = {};
	memset(&charlist, 0, sizeof(charlist));

	int filecount = 0;
	vector<string> listFiles = get_all_files_names_within_folder("charsave");
	for (auto str : listFiles)
	{
		string FileName = str;
		str = string("charsave\\") + str;

		if (ChoseChar >= 0 && ChoseChar <= 1)
		{
			if (ChoseChar)
			{
				charlist[filecount] = parsefile(str.c_str());
				if (charlist[filecount] != NULL) {
					if (verbose == 1)
						dumpchar(charlist[filecount]);
					filecount++;
				}
			}
			else
			{
				character* singlechar = parsefile(str.c_str());
				if (singlechar != NULL)
					dumpchar(singlechar);
			}
		}

		if (ChoseChar >= 3 && ChoseChar <= 4)
		{
			FILE* pFile = fopen(str.c_str(), "rb");
			if (pFile)
			{
				fseek(pFile, 0, SEEK_END);
				auto size = ftell(pFile);
				fseek(pFile, 0, SEEK_SET);
				BYTE* data = (BYTE*)malloc(size);
				if (!data) continue;
				memset(data, 0, size);

				DWORD nbRead = fread(data, 1, size, pFile);
				fclose(pFile);

				SaveHeader* pSaveHeader = (SaveHeader*)data;
				if (!pSaveHeader) continue;

				if (pSaveHeader->dwHeaderMagic != 0xAA55AA55)
					continue;

				if (strlen(pSaveHeader->szName) <= 0)
					continue;

				if (ChoseChar == 3)
				{
					BYTE FlagOld = pSaveHeader->bSaveFlags;
					if (FlagOld & 1)
						continue;

					pSaveHeader->bSaveFlags &= ~PLRFLAG_LADDER;
					char szBuffer[4096] = {};
					sprintf(szBuffer, "%s: Ladder -> Non-Ladder [0x%X -> 0x%X]", pSaveHeader->szName, FlagOld, pSaveHeader->bSaveFlags);
					cout << szBuffer << endl;
					Logcharsave(szBuffer);
				}

				if (ChoseChar == 4)
				{
					BYTE FlagOld = pSaveHeader->bSaveFlags;
					if (FlagOld & 1)
						continue;

					pSaveHeader->bSaveFlags |= PLRFLAG_LADDER;
					char szBuffer[4096] = {};
					sprintf(szBuffer, "%s: Non-Ladder -> Ladder [0x%X -> 0x%X]", pSaveHeader->szName, FlagOld, pSaveHeader->bSaveFlags);
					cout << szBuffer << endl;
					Logcharsave(szBuffer);
				}

				pSaveHeader->dwChecksum = d2charsave_checksum(data, pSaveHeader->dwSize, 0x0C);

				pFile = fopen(str.c_str(), "wb+");
				fwrite(data, size, 1, pFile);
				fclose(pFile);
			}
		}

		if (ChoseChar == 7)
		{
			FILE* pFile = fopen(str.c_str(), "rb");
			if (pFile)
			{
				fseek(pFile, 0, SEEK_END);
				auto size = ftell(pFile);
				fseek(pFile, 0, SEEK_SET);
				BYTE* data = (BYTE*)malloc(size);
				if (!data) continue;
				memset(data, 0, size);

				DWORD nbRead = fread(data, 1, size, pFile);
				fclose(pFile);

				SaveHeader* pSaveHeader = (SaveHeader*)data;
				if (!pSaveHeader) continue;

				if (pSaveHeader->dwHeaderMagic != 0xAA55AA55)
					continue;

				if (strlen(pSaveHeader->szName) <= 0)
					continue;

				string szName = addtext + string(pSaveHeader->szName);
				if (szName.empty())
					continue;

				if (szName.length() > 15)
				{
					char szBuffer[4096] = {};
					sprintf(szBuffer, "[%s] + [%s] Error Long Len: %d", pSaveHeader->szName, addtext.c_str(), szName.length());
					cout << szBuffer << endl;
					Logcharsave_error(szBuffer);
					continue;
				}

				string NameOLD = pSaveHeader->szName;
				strcpy(pSaveHeader->szName, NameOLD.insert(nSokytuthem, addtext).c_str());
				pSaveHeader->dwChecksum = d2charsave_checksum(data, pSaveHeader->dwSize, 0x0C);

				char szBuffer[4096] = {};
				sprintf(szBuffer, "[%s] + [%s] Pos: %d -> %s", FileName.c_str(), addtext.c_str(), nSokytuthem, pSaveHeader->szName);
				cout << szBuffer << endl;
				Logcharsave(szBuffer);

				DeleteFile(str.c_str());
				string NewFile = string("charsave\\") + string(pSaveHeader->szName);
				pFile = fopen(NewFile.c_str(), "wb+");
				fwrite(data, size, 1, pFile);
				fclose(pFile);
			}
		}
	}	
	
	vector<string> listFolder;
	getSubdirsRecursive(listFolder, "charinfo", "charinfo\\");
	for (auto strFolder : listFolder)
	{
		//Debug
		/*char szBuffer[4096] = {};
		sprintf(szBuffer, "Folder: %s", strFolder.c_str());
		cout << szBuffer << endl;
		Logcharinfo(szBuffer);*/

		vector<string> listFilescharinfo = get_all_files_names_within_folder(strFolder);
		for (auto str : listFilescharinfo)
		{
			string FileName = str;
			string strDir = string(strFolder) + string("\\") + str;
			if (ChoseChar >= 5 && ChoseChar <= 6)
			{
				FILE* pFile = fopen(strDir.c_str(), "rb");
				if (pFile)
				{
					fseek(pFile, 0, SEEK_END);
					auto size = ftell(pFile);
					fseek(pFile, 0, SEEK_SET);
					BYTE* data = (BYTE*)malloc(size);
					if (!data) continue;
					memset(data, 0, size);

					DWORD nbRead = fread(data, 1, size, pFile);
					fclose(pFile);

					t_d2charinfo_file* pCharInfo = (t_d2charinfo_file*)data;
					if (!pCharInfo) continue;

					if (pCharInfo->header.magicword != D2CHARINFO_MAGICWORD)
						continue;

					if (strlen(pCharInfo->header.charname) <= 0)
						continue;

					if (ChoseChar == 5)
					{
						auto gladder = charstatus_get_ladder(pCharInfo->summary.charstatus);
						charstatus_set_ladder(pCharInfo->summary.charstatus, 0);
						charstatus_set_ladder(pCharInfo->portrait.status, 0);
						pCharInfo->portrait.ladder = D2CHARINFO_PORTRAIT_PADBYTE;

						char szBuffer[4096] = {};
						sprintf(szBuffer, "%s: Ladder -> Non-Ladder [0x%X -> 0x%X]", pCharInfo->header.charname, gladder, charstatus_get_ladder(pCharInfo->summary.charstatus));
						cout << szBuffer << endl;
						Logcharinfo(szBuffer);
					}

					if (ChoseChar == 6)
					{
						auto gladder = charstatus_get_ladder(pCharInfo->summary.charstatus);
						charstatus_set_ladder(pCharInfo->summary.charstatus, 1);
						charstatus_set_ladder(pCharInfo->portrait.status, 1);
						pCharInfo->portrait.ladder = D2CHARINFO_PORTRAIT_PADBYTE;

						char szBuffer[4096] = {};
						sprintf(szBuffer, "%s: Non-Ladder -> Ladder [0x%X -> 0x%X]", pCharInfo->header.charname, gladder, charstatus_get_ladder(pCharInfo->summary.charstatus));
						cout << szBuffer << endl;
						Logcharinfo(szBuffer);
					}

					pFile = fopen(strDir.c_str(), "wb+");
					fwrite(data, size, 1, pFile);
					fclose(pFile);
				}
			}

			if (ChoseChar == 8)
			{
				FILE* pFile = fopen(strDir.c_str(), "rb");
				if (pFile)
				{
					fseek(pFile, 0, SEEK_END);
					auto size = ftell(pFile);
					fseek(pFile, 0, SEEK_SET);
					BYTE* data = (BYTE*)malloc(size);
					if (!data) continue;
					memset(data, 0, size);

					DWORD nbRead = fread(data, 1, size, pFile);
					fclose(pFile);

					t_d2charinfo_file* pCharInfo = (t_d2charinfo_file*)data;
					if (!pCharInfo) continue;

					if (pCharInfo->header.magicword != D2CHARINFO_MAGICWORD)
						continue;

					if (strlen(pCharInfo->header.charname) <= 0)
						continue;

					string szName = addtext + string(pCharInfo->header.charname);
					if (szName.empty())
						continue;

					if (szName.length() > 15)
					{
						char szBuffer[4096] = {};
						sprintf(szBuffer, "[%s] + [%s] Error Long Len: %d", pCharInfo->header.charname, addtext.c_str(), szName.length());
						cout << szBuffer << endl;
						Logcharinfo_error(szBuffer);
						continue;
					}

					string NameOLD = pCharInfo->header.charname;
					strcpy(pCharInfo->header.charname, NameOLD.insert(nSokytuthem, addtext).c_str());
	
					char szBuffer[4096] = {};
					sprintf(szBuffer, "[%s] + [%s] Pos: %d -> %s", FileName.c_str(), addtext.c_str(), nSokytuthem, pCharInfo->header.charname);
					cout << szBuffer << endl;
					Logcharinfo(szBuffer);

					DeleteFile(strDir.c_str());
					string NewFile = string(strFolder) + string("\\") + string(pCharInfo->header.charname);
					pFile = fopen(NewFile.c_str(), "wb+");
					fwrite(data, size, 1, pFile);
					fclose(pFile);
				}
			}
		}
	}

	if (ChoseChar == 1)
	{
		for (int i = 0; i < filecount - 1; i++)
		{
			for (int j = i + 1; j < filecount; j++)
			{
				char_compare(charlist[i], charlist[j]);
			}
		}
	}

	system("pause");
	return 0;
}